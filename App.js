import React, {Component} from 'react';
import { View } from "react-native"
import InsertData from './src/InsertData'
import ViewData from './src/ViewData'
import './src/config/FixTime'

export default class App extends Component {
    render() {
        return (
            <View>
                <InsertData/>
                <ViewData/>
            </View>
        )
    }
}
import React, {useState, useEffect} from 'react'
import {Text, View, FlatList} from 'react-native'
import {ListItem} from 'react-native-elements'
import {db} from './config/config'

const ViewData = () => {
    let dataFirebase = db.ref('items')
    const [data, setData] = useState()

    useEffect(() => {
        dataFirebase.on('value', snapshot => {
            let data = snapshot.val()
            let items = Object.values(data)
            setData(items)
        })
    }), [setData]

    return (
        <View>
            {data != null ? (
                <FlatList
                    data={data}
                    renderItem={({item}) => (
                        <ListItem
                            title={item.name}
                            subtitle={item.name}
                            onPress={()=>{
                                alert('Ini Idnya ',item.id)
                            }}
                        />

                    )}
                />
            ):(
                <Text>Loading</Text>
            )}

        </View>
    )
}

export default ViewData


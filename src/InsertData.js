import React, { useState } from 'react'
import {Button, Text, TextInput, View} from "react-native"
import { db } from './config/config'

const InsertData = () => {
    let [name, setName] = useState("")
    let addItem = ( id,item ) => {
        db.ref('/items').push({
            name: item,
            id : id,
        })
    }

    let handleSubmit = () => {
        addItem(Date.now(), name)
        alert(name)
    }

    return(
        <View>
            <Text>Insert Nama</Text>
            <TextInput
                placeholder="Masukkan nama"
                onChangeText={value=>setName(value)}
            />
            <Button
                title={"Masukan data"}
                onPress={handleSubmit}
            />
        </View>
    )
}

export default InsertData